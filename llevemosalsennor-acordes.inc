\context ChordNames
	\chords {
		\set majorSevenSymbol = \markup { "maj7" }
		\set chordChanges = ##t

		% intro
		e4.:m e4.:m e4.:m e4.:m

		% llevemos al sennor...
		e4.:m e4.:m e4.:m e4.:m
		d4. e4.:m e4.:m e4.:m e4.:m
		e4.:m e4.:m e4.:m e4.:m
		d4. e4.:m e4.:m e4.:m e4.:m

		% el sennor nos dara...
		g4. g4. d4. g4.
		d4. e4.:m e4.:m e4.:m e4.:m
		g4. g4. d4. g4.
		d4. e4.:m e4.:m e4.:m e4.:m

		% llevemos al sennor...
		e4.:m e4.:m e4.:m e4.:m
		d4. e4.:m e4.:m e4.:m e4.:m
		e4.:m e4.:m e4.:m e4.:m
		d4. e4.:m e4.:m e4.:m e4.:m

		% el sennor nos dara...
		g4. g4. d4. g4.
		d4. e4.:m e4.:m e4.:m e4.:m
		g4. g4. d4. g4.
		d4. e4.:m e4.:m e4.:m e4.:m

		% llevemos al sennor...
		e4.:m e4.:m e4.:m e4.:m
		d4. e4.:m e4.:m e4.:m e4.:m
		e4.:m e4.:m e4.:m e4.:m
		d4. e4.:m e4.:m e4.:m e4.:m

		% el sennor nos dara...
		g4. g4. d4. g4.
		d4. e4.:m e4.:m e4.:m e4.:m
		g4. g4. d4. g4.
		d4. e4.:m e4.:m e4.:m e4.:m
		g4. g4. d4. g4.
		d4. e4.:m e4.:m e4.:m e4.:m
	}

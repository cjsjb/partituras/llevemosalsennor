\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 3/8
		\clef "treble"
		\key e \minor

		R4.*3  |
		r8 r b  |
%% 5
		e' 4 e' 8  |
		e' 4 d' 8  |
		e' 4. ~  |
		e' 8 r fis'  |
		fis' 8. g' 16 fis' 8  |
%% 10
		e' 4. ~  |
		e' 4. ~  |
		e' 8 r r  |
		r8 r b  |
		e' 4 e' 8  |
%% 15
		e' 4 d' 8  |
		e' 4. ~  |
		e' 8 r fis'  |
		fis' 8. g' 16 fis' 8  |
		e' 4. ~  |
%% 20
		e' 4. ~  |
		e' 8 r r  |
		g' 4 a' 8  |
		b' 4.  |
		b' 4 b' 8  |
%% 25
		a' 4.  |
		b' 8. a' 16 g' 8  |
		fis' 8. e' 16 d' 8  |
		e' 4. ~  |
		e' 4. ~  |
%% 30
		e' 4 r8  |
		g' 4 a' 8  |
		b' 4.  |
		b' 4 b' 8  |
		a' 4.  |
%% 35
		b' 8. a' 16 g' 8  |
		fis' 8. e' 16 d' 8  |
		e' 4. ~  |
		e' 4. ~  |
		e' 4 r8  |
%% 40
		r8 r b  |
		e' 4 e' 8  |
		e' 4 d' 8  |
		e' 4. ~  |
		e' 8 r fis'  |
%% 45
		fis' 8. g' 16 fis' 8  |
		e' 4. ~  |
		e' 4. ~  |
		e' 8 r r  |
		r8 r b  |
%% 50
		e' 4 e' 8  |
		e' 4 d' 8  |
		e' 4. ~  |
		e' 8 r fis'  |
		fis' 8. g' 16 fis' 8  |
%% 55
		e' 4. ~  |
		e' 4. ~  |
		e' 8 r r  |
		g' 4 a' 8  |
		b' 4.  |
%% 60
		b' 4 b' 8  |
		a' 4.  |
		b' 8. a' 16 g' 8  |
		fis' 8. e' 16 d' 8  |
		e' 4. ~  |
%% 65
		e' 4. ~  |
		e' 4 r8  |
		g' 4 a' 8  |
		b' 4.  |
		b' 4 b' 8  |
%% 70
		a' 4.  |
		b' 8. a' 16 g' 8  |
		fis' 8. e' 16 d' 8  |
		e' 4. ~  |
		e' 4. ~  |
%% 75
		e' 4 r8  |
		r8 r b  |
		e' 4 e' 8  |
		e' 4 d' 8  |
		e' 4. ~  |
%% 80
		e' 8 r fis'  |
		fis' 8. g' 16 fis' 8  |
		e' 4. ~  |
		e' 4. ~  |
		e' 8 r r  |
%% 85
		r8 r b  |
		e' 4 e' 8  |
		e' 4 d' 8  |
		e' 4. ~  |
		e' 8 r fis'  |
%% 90
		fis' 8. g' 16 fis' 8  |
		e' 4. ~  |
		e' 4. ~  |
		e' 8 r r  |
		g' 4 a' 8  |
%% 95
		b' 4.  |
		b' 4 b' 8  |
		a' 4.  |
		b' 8. a' 16 g' 8  |
		fis' 8. e' 16 d' 8  |
%% 100
		e' 4. ~  |
		e' 4. ~  |
		e' 4 r8  |
		g' 4 a' 8  |
		b' 4.  |
%% 105
		b' 4 b' 8  |
		a' 4.  |
		b' 8. a' 16 g' 8  |
		fis' 8. e' 16 d' 8  |
		e' 4. ~  |
%% 110
		e' 4. ~  |
		e' 4 r8  |
		g' 4 a' 8  |
		b' 4.  |
		b' 4 b' 8  |
%% 115
		a' 4.  |
		b' 8. a' 16 g' 8  |
		fis' 8. e' 16 d' 8  |
		e' 4. ~  |
		e' 4. ~  |
%% 120
		e' 4. ~  |
		e' 4.  |
		R4.  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		Lle -- ve -- mos al Se -- ñor __
		el vi -- no "y el" pan. __
		Lle -- ve -- mos al al -- tar __
		la vi -- "ña, el" tri -- gal. __

		El Se -- ñor nos da -- rá, Él nos da -- rá "su a" -- mis -- tad. __
		El Se -- ñor nos da -- rá, Él nos da -- rá "su a" -- mis -- tad. __

		Lle -- ve -- mos al Se -- ñor __
		pu -- re -- za "y a" -- mor. __
		Lle -- ve -- mos al al -- tar __
		jus -- ti -- "cia, her" -- man -- dad. __

		El Se -- ñor nos da -- rá, Él nos da -- rá "su a" -- mis -- tad. __
		El Se -- ñor nos da -- rá, Él nos da -- rá "su a" -- mis -- tad. __

		Lle -- ve -- mos al Se -- ñor tra -- ba -- "jo y" do -- lor.
		Lle -- ve -- mos al al -- tar o -- fren -- das de paz.

		El Se -- ñor nos da -- rá, Él nos da -- rá "su a" -- mis -- tad. __
		El Se -- ñor nos da -- rá, Él nos da -- rá "su a" -- mis -- tad. __
		El Se -- ñor nos da -- rá, Él nos da -- rá "su a" -- mis -- tad. __
	}
>>

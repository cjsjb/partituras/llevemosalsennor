\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 3/8
		\clef "treble_8"
		\key e \minor

		R4.*3  |
		r8 r b,  |
%% 5
		e 4 e 8  |
		e 4 d 8  |
		e 4. ~  |
		e 8 r fis  |
		fis 8. g 16 fis 8  |
%% 10
		e 4. ~  |
		e 4. ~  |
		e 8 r r  |
		r8 r b,  |
		e 4 e 8  |
%% 15
		e 4 d 8  |
		e 4. ~  |
		e 8 r a  |
		a 8. b 16 a 8  |
		g 4. ~  |
%% 20
		g 4. ~  |
		g 8 r r  |
		g 4 a 8  |
		d' 4.  |
		d' 4 d' 8  |
%% 25
		d' 4.  |
		d' 8. c' 16 b 8  |
		a 8. g 16 fis 8  |
		g 4. ~  |
		g 4. ~  |
%% 30
		g 4 r8  |
		g 4 a 8  |
		d' 4.  |
		d' 4 d' 8  |
		d' 4.  |
%% 35
		d' 8. c' 16 b 8  |
		a 8. g 16 fis 8  |
		g 4. ~  |
		g 4. ~  |
		g 4 r8  |
%% 40
		r8 r b,  |
		e 4 e 8  |
		e 4 d 8  |
		e 4. ~  |
		e 8 r fis  |
%% 45
		fis 8. g 16 fis 8  |
		e 4. ~  |
		e 4. ~  |
		e 8 r r  |
		r8 r b,  |
%% 50
		e 4 e 8  |
		e 4 d 8  |
		e 4. ~  |
		e 8 r a  |
		a 8. b 16 a 8  |
%% 55
		g 4. ~  |
		g 4. ~  |
		g 8 r r  |
		g 4 a 8  |
		d' 4.  |
%% 60
		d' 4 d' 8  |
		d' 4.  |
		d' 8. c' 16 b 8  |
		a 8. g 16 fis 8  |
		g 4. ~  |
%% 65
		g 4. ~  |
		g 4 r8  |
		g 4 a 8  |
		d' 4.  |
		d' 4 d' 8  |
%% 70
		d' 4.  |
		d' 8. c' 16 b 8  |
		a 8. g 16 fis 8  |
		g 4. ~  |
		g 4. ~  |
%% 75
		g 4 r8  |
		r8 r b,  |
		e 4 e 8  |
		e 4 d 8  |
		e 4. ~  |
%% 80
		e 8 r fis  |
		fis 8. g 16 fis 8  |
		e 4. ~  |
		e 4. ~  |
		e 8 r r  |
%% 85
		r8 r b,  |
		e 4 e 8  |
		e 4 d 8  |
		e 4. ~  |
		e 8 r a  |
%% 90
		a 8. b 16 a 8  |
		g 4. ~  |
		g 4. ~  |
		g 8 r r  |
		g 4 a 8  |
%% 95
		d' 4.  |
		d' 4 d' 8  |
		d' 4.  |
		d' 8. c' 16 b 8  |
		a 8. g 16 fis 8  |
%% 100
		g 4. ~  |
		g 4. ~  |
		g 4 r8  |
		g 4 a 8  |
		d' 4.  |
%% 105
		d' 4 d' 8  |
		d' 4.  |
		d' 8. c' 16 b 8  |
		a 8. g 16 fis 8  |
		g 4. ~  |
%% 110
		g 4. ~  |
		g 4 r8  |
		g 4 a 8  |
		d' 4.  |
		d' 4 d' 8  |
%% 115
		d' 4.  |
		d' 8. c' 16 b 8  |
		a 8. g 16 fis 8  |
		g 4. ~  |
		g 4. ~  |
%% 120
		g 4. ~  |
		g 4.  |
		R4.  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		Lle -- ve -- mos al Se -- ñor __
		el vi -- no "y el" pan. __
		Lle -- ve -- mos al al -- tar __
		la vi -- "ña, el" tri -- gal. __

		El Se -- ñor nos da -- rá, Él nos da -- rá "su a" -- mis -- tad. __
		El Se -- ñor nos da -- rá, Él nos da -- rá "su a" -- mis -- tad. __

		Lle -- ve -- mos al Se -- ñor __
		pu -- re -- za "y a" -- mor. __
		Lle -- ve -- mos al al -- tar __
		jus -- ti -- "cia, her" -- man -- dad. __

		El Se -- ñor nos da -- rá, Él nos da -- rá "su a" -- mis -- tad. __
		El Se -- ñor nos da -- rá, Él nos da -- rá "su a" -- mis -- tad. __

		Lle -- ve -- mos al Se -- ñor tra -- ba -- "jo y" do -- lor.
		Lle -- ve -- mos al al -- tar o -- fren -- das de paz.

		El Se -- ñor nos da -- rá, Él nos da -- rá "su a" -- mis -- tad. __
		El Se -- ñor nos da -- rá, Él nos da -- rá "su a" -- mis -- tad. __
		El Se -- ñor nos da -- rá, Él nos da -- rá "su a" -- mis -- tad. __
	}
>>

\context Staff = "mezzosoprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzosoprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 3/8
		\clef "treble"
		\key e \minor

		R4.*3  |
		r8 r b  |
%% 5
		e' 4 e' 8  |
		e' 4 d' 8  |
		e' 4. ~  |
		e' 8 r d'  |
		d' 8. e' 16 d' 8  |
%% 10
		b 4. ~  |
		b 4. ~  |
		b 8 r r  |
		r8 r b  |
		e' 4 e' 8  |
%% 15
		e' 4 d' 8  |
		e' 4. ~  |
		e' 8 r d'  |
		d' 8. e' 16 d' 8  |
		b 4. ~  |
%% 20
		b 4. ~  |
		b 8 r r  |
		e' 4 fis' 8  |
		g' 4.  |
		g' 4 g' 8  |
%% 25
		fis' 4.  |
		g' 8. fis' 16 e' 8  |
		d' 8. b 16 a 8  |
		b 4. ~  |
		b 4. ~  |
%% 30
		b 4 r8  |
		e' 4 fis' 8  |
		g' 4.  |
		g' 4 g' 8  |
		fis' 4.  |
%% 35
		g' 8. fis' 16 e' 8  |
		d' 8. b 16 a 8  |
		b 4. ~  |
		b 4. ~  |
		b 4 r8  |
%% 40
		r8 r b  |
		e' 4 e' 8  |
		e' 4 d' 8  |
		e' 4. ~  |
		e' 8 r d'  |
%% 45
		d' 8. e' 16 d' 8  |
		b 4. ~  |
		b 4. ~  |
		b 8 r r  |
		r8 r b  |
%% 50
		e' 4 e' 8  |
		e' 4 d' 8  |
		e' 4. ~  |
		e' 8 r d'  |
		d' 8. e' 16 d' 8  |
%% 55
		b 4. ~  |
		b 4. ~  |
		b 8 r r  |
		e' 4 fis' 8  |
		g' 4.  |
%% 60
		g' 4 g' 8  |
		fis' 4.  |
		g' 8. fis' 16 e' 8  |
		d' 8. b 16 a 8  |
		b 4. ~  |
%% 65
		b 4. ~  |
		b 4 r8  |
		e' 4 fis' 8  |
		g' 4.  |
		g' 4 g' 8  |
%% 70
		fis' 4.  |
		g' 8. fis' 16 e' 8  |
		d' 8. b 16 a 8  |
		b 4. ~  |
		b 4. ~  |
%% 75
		b 4 r8  |
		r8 r b  |
		e' 4 e' 8  |
		e' 4 d' 8  |
		e' 4. ~  |
%% 80
		e' 8 r d'  |
		d' 8. e' 16 d' 8  |
		b 4. ~  |
		b 4. ~  |
		b 8 r r  |
%% 85
		r8 r b  |
		e' 4 e' 8  |
		e' 4 d' 8  |
		e' 4. ~  |
		e' 8 r d'  |
%% 90
		d' 8. e' 16 d' 8  |
		b 4. ~  |
		b 4. ~  |
		b 8 r r  |
		e' 4 fis' 8  |
%% 95
		g' 4.  |
		g' 4 g' 8  |
		fis' 4.  |
		g' 8. fis' 16 e' 8  |
		d' 8. b 16 a 8  |
%% 100
		b 4. ~  |
		b 4. ~  |
		b 4 r8  |
		e' 4 fis' 8  |
		g' 4.  |
%% 105
		g' 4 g' 8  |
		fis' 4.  |
		g' 8. fis' 16 e' 8  |
		d' 8. b 16 a 8  |
		b 4. ~  |
%% 110
		b 4. ~  |
		b 4 r8  |
		e' 4 fis' 8  |
		g' 4.  |
		g' 4 g' 8  |
%% 115
		fis' 4.  |
		g' 8. fis' 16 e' 8  |
		d' 8. b 16 a 8  |
		b 4. ~  |
		b 4. ~  |
%% 120
		b 4. ~  |
		b 4.  |
		R4.  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzosoprano" {
		Lle -- ve -- mos al Se -- ñor __
		el vi -- no "y el" pan. __
		Lle -- ve -- mos al al -- tar __
		la vi -- "ña, el" tri -- gal. __

		El Se -- ñor nos da -- rá, Él nos da -- rá "su a" -- mis -- tad. __
		El Se -- ñor nos da -- rá, Él nos da -- rá "su a" -- mis -- tad. __

		Lle -- ve -- mos al Se -- ñor __
		pu -- re -- za "y a" -- mor. __
		Lle -- ve -- mos al al -- tar __
		jus -- ti -- "cia, her" -- man -- dad. __

		El Se -- ñor nos da -- rá, Él nos da -- rá "su a" -- mis -- tad. __
		El Se -- ñor nos da -- rá, Él nos da -- rá "su a" -- mis -- tad. __

		Lle -- ve -- mos al Se -- ñor tra -- ba -- "jo y" do -- lor.
		Lle -- ve -- mos al al -- tar o -- fren -- das de paz.

		El Se -- ñor nos da -- rá, Él nos da -- rá "su a" -- mis -- tad. __
		El Se -- ñor nos da -- rá, Él nos da -- rá "su a" -- mis -- tad. __
		El Se -- ñor nos da -- rá, Él nos da -- rá "su a" -- mis -- tad. __
	}
>>
